package asi.controller;

import asi.MQServices.Sender;
import asi.model.CardDTO;
import asi.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private Sender sender;

	public List<UserModel> getAllUsers() {
		System.out.println("[UserService] UserService#getAllUsers");
		List<UserModel> userList = new ArrayList<>();
		userRepository.findAll().forEach(userList::add);
		return userList;
	}

	public Optional<UserModel> getUser(String id) {
		System.out.println("[UserService] UserService#getUser");
		return userRepository.findById(Integer.valueOf(id));
	}

	public Optional<UserModel> getUser(Integer id) {
		System.out.println("[UserService] UserService#getUser");
		return userRepository.findById(id);
	}

	public void addUser(UserModel user) {
		System.out.println("[UserService] UserService#addUser");
		// needed to avoid detached entity passed to persist error
		UserModel saved = userRepository.save(user);

		sender.sendMessageCreateRandomCard(5, saved.getId());
	}

	public void updateUser(UserModel user) {
		System.out.println("[UserService] UserService#updateUser");
		userRepository.save(user);
	}

	public void deleteUser(String id) {
		System.out.println("[UserService] UserService#deleteUser");
		userRepository.deleteById(Integer.valueOf(id));
	}

	public List<UserModel> getUserByLoginPwd(String login, String pwd) {
		System.out.println("[UserService] UserService#getUserByLoginPwd");
		List<UserModel> ulist=null;
		ulist=userRepository.findByLoginAndPwd(login,pwd);
		return ulist;
	}

}
