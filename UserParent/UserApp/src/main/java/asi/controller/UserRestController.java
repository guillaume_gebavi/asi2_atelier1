package asi.controller;

import asi.MQServices.Sender;
import asi.model.CardDTO;
import asi.model.UserModel;
import model.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class UserRestController {
	
	@Autowired
	private UserService userService;

	@Autowired
	private Sender sender;
	
	@RequestMapping("/users")
	private List<UserDTO> getAllUsers() {
		System.out.println("[USerService] UserRestController#getAllUsers");
		List<UserDTO> listDto = new ArrayList<>();

		for (UserModel model : userService.getAllUsers())
		{
			Set<Integer> cards = sender.getAllCardForUser(model.getId());
			List<CardDTO> cardsDTO = sender.getAllCardForUserDto(model.getId());

			listDto.add(new UserDTO(model.getId(), model.getLogin(), "", model.getAccount(), model.getLastName(),
					model.getSurName(), model.getEmail(), cards, cardsDTO));
		}

		return listDto;

	}
	
	@RequestMapping("/user/{id}")
	private UserDTO getUser(@PathVariable String id) {
		System.out.println("[USerService] UserRestController#getAllUsers - param " + id);
		Optional<UserModel> ruser;
		ruser= userService.getUser(id);
		if(ruser.isPresent()) {
			UserModel model =  ruser.get();

			Set<Integer> cards = sender.getAllCardForUser(model.getId());
			List<CardDTO> cardsDTO = sender.getAllCardForUserDto(model.getId());

			return new UserDTO(model.getId(), model.getLogin(), "", model.getAccount(), model.getLastName(),
					model.getSurName(), model.getEmail(), cards, cardsDTO);
		}
		return null;

	}
	
	@RequestMapping(method= RequestMethod.POST,value="/user")
	public void addUser(@RequestBody UserDTO user) {
		System.out.println("[USerService] UserRestController#addUser - param " + user);
		userService.addUser(new UserModel(user));
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/user/{id}")
	public void updateUser(@RequestBody UserDTO user,@PathVariable String id) {
		System.out.println("[USerService] UserRestController#updateUser - params " + user + " | " + id );
		user.setId(Integer.valueOf(id));
		userService.updateUser(new UserModel(user));
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/user/{id}")
	public void deleteUser(@PathVariable String id) {
		System.out.println("[USerService] UserRestController#deleteUser - params " + id );
		userService.deleteUser(id);
	}


	@RequestMapping(method=RequestMethod.POST,value="/auth")
	private Integer getAuth(@RequestBody UserDTO user) {
		System.out.println("[USerService] UserRestController#getAuth");
		List<UserModel> users = userService.getUserByLoginPwd(user.getLogin(), user.getPwd());


		if(!users.isEmpty()) {
			return users.get(0).getId();
		}
		return null;
	}

	@RequestMapping(method=RequestMethod.GET,value="/authWithUser")
	private UserDTO getAuthWithUser(@RequestParam("login") String login, @RequestParam("pwd") String pwd) {
		System.out.println("[USerService] UserRestController#getAuthWithUser");
		List<UserModel> users = userService.getUserByLoginPwd(login, pwd);


		if(!users.isEmpty()) {
			UserModel model =  users.get(0);
			return new UserDTO(model.getId(), model.getLogin(), "", model.getAccount(), model.getLastName(),
					model.getSurName(), model.getEmail(), null, null);
		}

		return null;
	}

}
