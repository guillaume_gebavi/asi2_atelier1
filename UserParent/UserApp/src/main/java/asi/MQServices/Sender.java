package asi.MQServices;

import asi.model.CardDTO;
import dto.EnveloppeDto;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import model.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import services.AbstractSender;

import java.util.*;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Data
public class Sender extends AbstractSender {

    public static final Integer TIME_WAIT = 1000;

    public static EnveloppeDto response = null;

    private static final String QUEUE_KEY = "User.Create.GetCard.queue";
    @Override
    public String getQueueKey() {
        return QUEUE_KEY;
    }


    public void sendMessageUserId(Integer userId) {

        EnveloppeDto dto = new EnveloppeDto();
        dto.setObject(userId);
        //dto.setObjectType(cardDTO.getClass().getName());

        // Send a message
        System.out.println("Sending an Integer message.");
        super.sendMessage(dto, "User.Create.AskCard.queue"); // TODO
    }

    public Set<Integer> getAllCardForUser(Integer userId)
    {
        EnveloppeDto dto = new EnveloppeDto();
        dto.setObject(userId);

        System.out.println("[UserService] [ActiveMQ] Envoi du message sur 'User.GetAllCards.queue' : " + userId);
        super.sendMessage(dto, "User.GetAllCards.queue");

        System.out.println("[UserService] [ActiveMQ] Attente d'une réponse ");
        try {
            Thread.sleep(TIME_WAIT);
            if (response != null && response.getObject() instanceof Set)
            {
                System.out.println("[UserService] [ActiveMQ] traitement de la réponse ");
                Set<Integer> result =  (Set<Integer>) response.getObject();
                response = null;
                return result;
            }
        } catch (InterruptedException e) {
            System.err.println(e);
        }

        return null;
    }

    public List<CardDTO> getAllCardForUserDto(Integer userId)
    {
        EnveloppeDto dto = new EnveloppeDto();
        dto.setObject(userId);

        System.out.println("[UserService] [ActiveMQ] Envoi du message sur 'User.GetAllCards.DTO.queue' : " + userId);
        super.sendMessage(dto, "User.GetAllCards.DTO.queue");

        System.out.println("[UserService] [ActiveMQ] Attente d'une réponse ");
        try {
            Thread.sleep(TIME_WAIT);
            if (response != null && response.getObject() instanceof List)
            {
                System.out.println("[UserService] [ActiveMQ] traitement de la réponse ");
                List<CardDTO> result =  (List<CardDTO>) response.getObject();
                response = null;
                return result;
            }
        } catch (InterruptedException e) {
            System.err.println(e);
        }

        return null;
    }

    public void sendMessageCreateRandomCard(Integer nb, Integer user)
    {
        EnveloppeDto dto = new EnveloppeDto();
        Map<String, Integer> map = new HashMap<>();
        map.put("nb", nb);
        map.put("id", user);
        dto.setObject(map);

        System.out.println("[UserService] [ActiveMQ] Envoi du message sur 'User.Create.AskCard.queue' : " + map);
        super.sendMessage(dto, "User.Create.AskCard.queue");

        System.out.println("[UserService] [ActiveMQ] Attente d'une réponse ");
        try {
            Thread.sleep(TIME_WAIT);
            if (response != null && response.getObject() instanceof HashSet)
            {
                System.out.println("[UserService] [ActiveMQ] traitement de la réponse ");
                System.out.println(response.getObject());
                response = null;
            }
        } catch (InterruptedException e) {
            System.err.println(e);
        }

    }

    public void sendMessageUserDto(UserDTO userDTO) {
        EnveloppeDto dto = new EnveloppeDto();
        dto.setObject(userDTO);

        // Send a message
        System.out.println("[UserService] [ActiveMQ] Envoi du message sur 'User.getUser.Response.queue' : " + userDTO);
        super.sendMessage(dto, "User.getUser.Response.queue");
    }
}
