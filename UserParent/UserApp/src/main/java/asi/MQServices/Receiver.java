package asi.MQServices;

import asi.controller.UserService;
import asi.model.CardDTO;
import asi.model.UserModel;
import dto.EnveloppeDto;
import lombok.extern.log4j.Log4j2;
import model.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class Receiver {

    @Autowired
    private Sender sender;

    @Autowired
    private UserService userService;

    @JmsListener(destination = "User.Create.AskCard.Response.queue")
    public void receiveMessage1(EnveloppeDto dto) {

        System.out.println("[UserService] [ActiveMQ] Réception message sur 'User.Create.AskCard.Response.queue' : " + dto);

        log.info(dto);
        Sender.response = dto;

    }


    @JmsListener(destination = "User.GetAllCards.Response.queue")
    public void receiveMessage2(EnveloppeDto dto) {

        System.out.println("[UserService] [ActiveMQ] Réception message sur 'User.GetAllCards.Response.queue' : " + dto);

        log.info(dto);
        Sender.response = dto;

    }

    @JmsListener(destination = "User.GetAllCards.DTO.Response.queue")
    public void receiveMessage3(EnveloppeDto dto) {

        System.out.println("[UserService] [ActiveMQ] Réception message sur 'User.GetAllCards.DTO.Response.queue' : " + dto);

        log.info(dto);
        Sender.response = dto;

    }

    @JmsListener(destination = "User.updateUser.queue")
    public void receiveMessage4(EnveloppeDto dto) {

        System.out.println("[UserService] [ActiveMQ] Réception message sur 'User.updateUser.queue' : " + dto);

        log.info(dto);
        UserDTO userDTO = (UserDTO) dto.getObject();

        userService.updateUser(new UserModel(userDTO));

    }

    @JmsListener(destination = "User.getUser.queue")
    public void receiveMessage5(EnveloppeDto dto) {

        System.out.println("[UserService] [ActiveMQ] Réception message sur 'User.getUser.queue' : " + dto);

        log.info(dto);
        Integer userId = (Integer) dto.getObject();

        UserModel model = userService.getUser(userId).orElse(null);

        UserDTO userDTO =  new UserDTO(model.getId(), model.getLogin(), "", model.getAccount(), model.getLastName(),
                model.getSurName(), model.getEmail(), null, null);

        sender.sendMessageUserDto(userDTO);
    }
}
