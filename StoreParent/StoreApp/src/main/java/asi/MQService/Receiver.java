package asi.MQService;

import dto.EnveloppeDto;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class Receiver {

    @Autowired
    private Sender sender;


    @JmsListener(destination = "Cards.getAllCards.Response.queue")
    public void receiveMessage1(EnveloppeDto enveloppeDto) {
        System.out.println("[StoreService] [ActiveMQ] Réception message sur 'Cards.getAllCards.Response.queue' : " + enveloppeDto);
        Sender.response = enveloppeDto;
    }

    @JmsListener(destination = "Cards.getCardsToSell.Response.queue")
    public void receiveMessage2(EnveloppeDto enveloppeDto) {
        System.out.println("[StoreService] [ActiveMQ] Réception message sur 'Cards.getCardsToSell.Response.queue' : " + enveloppeDto);
        Sender.response = enveloppeDto;
    }

    @JmsListener(destination = "Cards.getAllCards.Response.queue")
    public void receiveMessage3(EnveloppeDto enveloppeDto) {
        System.out.println("[StoreService] [ActiveMQ] Réception message sur 'Cards.getAllCards.Response.queue' : " + enveloppeDto);
        Sender.response = enveloppeDto;
    }

    @JmsListener(destination = "User.getUser.Response.queue")
    public void receiveMessage4(EnveloppeDto enveloppeDto) {
        System.out.println("[StoreService] [ActiveMQ] Réception message sur 'User.getUser.Response.queue' : " + enveloppeDto);
        Sender.response = enveloppeDto;
    }

    @JmsListener(destination = "Card.getCard.Response.queue")
    public void receiveMessage5(EnveloppeDto enveloppeDto) {
        System.out.println("[StoreService] [ActiveMQ] Réception message sur 'Card.getCard.Response.queue' : " + enveloppeDto);
        Sender.response = enveloppeDto;
    }

}
