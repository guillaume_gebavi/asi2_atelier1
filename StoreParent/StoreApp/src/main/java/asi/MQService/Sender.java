package asi.MQService;

import asi.model.CardDTO;
import dto.EnveloppeDto;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import model.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import services.AbstractSender;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Data
public class Sender extends AbstractSender {

    public static EnveloppeDto response = null;

    public static final Integer TIME_WAIT = 3000;


    @Override
    public String getQueueKey() {
        return "";
    }

    public UserDTO getUserInfo(Integer userId) {
        EnveloppeDto dto = new EnveloppeDto();
        dto.setObject(userId);
        System.out.println("[StoreService] [ActiveMQ] Envoi du message sur 'User.getUser.queue' : " + userId);
        sendMessage(dto, "User.getUser.queue");

        System.out.println("[StoreService] [ActiveMQ] Attente d'une réponse ");
        try {
            Thread.sleep(TIME_WAIT);
            if (response != null && response.getObject() instanceof UserDTO) {
                System.out.println("[StoreService] [ActiveMQ] traitement de la réponse ");
                UserDTO result = (UserDTO) response.getObject();
                response = null;
                return result;
            }
        } catch (InterruptedException e) {
            System.err.println(e);
        }
        return null;
    }

    public CardDTO getCardInfo(Integer cardId) {
        EnveloppeDto dto = new EnveloppeDto();
        dto.setObject(cardId);
        System.out.println("[StoreService] [ActiveMQ] Envoi du message sur 'Cards.getCard.queue' : " + cardId);
        sendMessage(dto, "Cards.getCard.queue");

        System.out.println("[StoreService] [ActiveMQ] Attente d'une réponse ");
        try {
            Thread.sleep(TIME_WAIT);
            if (response != null && response.getObject() instanceof CardDTO) {
                System.out.println("[StoreService] [ActiveMQ] traitement de la réponse ");
                CardDTO result = (CardDTO) response.getObject();
                response = null;
                return result;
            }
        } catch (InterruptedException e) {
            System.err.println(e);
        }
        return null;
    }

    public List<CardDTO> getAllCardsInfo() {

        System.out.println("[StoreService] [ActiveMQ] Envoi du message sur 'Cards.getAllCards.queue'");
        sendMessage(new EnveloppeDto(), "Cards.getAllCards.queue");

        System.out.println("[StoreService] [ActiveMQ] Attente d'une réponse ");
        try {
            Thread.sleep(TIME_WAIT);
            if (response != null && response.getObject() instanceof List) {
                System.out.println("[StoreService] [ActiveMQ] traitement de la réponse ");
                List<CardDTO> result = (List<CardDTO>) response.getObject();
                response = null;
                return result;
            }
        } catch (InterruptedException e) {
            System.err.println(e);
        }
        return null;
    }

    public Boolean updateCard(CardDTO card) {
        EnveloppeDto dto = new EnveloppeDto();
        dto.setObject(card);

        System.out.println("[StoreService] [ActiveMQ] Envoi du message sur 'Cards.updateCard.queue' : " + card);
        sendMessage(dto, "Cards.updateCard.queue");
        return Boolean.TRUE; // TODO
    }

    public Boolean updateUser(UserDTO user) {
        EnveloppeDto dto = new EnveloppeDto();
        dto.setObject(user);

        System.out.println("[StoreService] [ActiveMQ] Envoi du message sur 'User.updateUser.queue' : " + user);
        sendMessage(dto, "User.updateUser.queue");
        return Boolean.TRUE; // TODO
    }
}
