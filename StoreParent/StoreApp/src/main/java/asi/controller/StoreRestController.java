package asi.controller;

import asi.model.CardDTO;
import model.StoreDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class StoreRestController {

	

	@Autowired
	private StoreService storeService;
	
	@RequestMapping(method= RequestMethod.POST,value="/buy")
	private boolean buy(@RequestBody StoreDto order) {
		System.out.println("[StoreService] StoreRestController#buy - Param : " + order);
		return storeService.buyCard(order.getUserId(),order.getCardId());

	}
	
	@RequestMapping(method=RequestMethod.POST,value="/sell")
	private boolean getCard(@RequestBody StoreDto order) {
		System.out.println("[StoreService] StoreRestController#sell - Param : " + order);
		 return storeService.sellCard(order.getUserId(),order.getCardId());
	}

	@GetMapping(value = "/CardsToBuy")
	private List<CardDTO> getCardsToSell()
	{
		System.out.println("[StoreService] StoreRestController#CardsToBuy");
		List<CardDTO> list = storeService.getAllStoreCard();
		if (list == null) list = new ArrayList<>();
		return list;
	}
	
}
