package asi.controller;

import asi.MQService.Sender;
import asi.model.CardDTO;
import model.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StoreService {

    @Autowired
    private Sender sender;

    public boolean buyCard(Integer userId, Integer cardId) {
        System.out.println("[StoreService] StoreService#buyCard");
        UserDTO user = sender.getUserInfo(userId);
        CardDTO card = sender.getCardInfo(cardId);
        if (user == null || card == null) {
            return false;
        }

        if (card.getUserId() != null)
            return false;

        boolean soldeOK = user.getAccount() > card.getPrice();
        if (soldeOK)
		{
			//user.getCardList().add(cardId); // Inutile
			user.setAccount(user.getAccount() - card.getPrice());
			card.setUserId(userId);
			sender.updateUser(user);
			sender.updateCard(card);
		}

		return soldeOK;
    }

    public boolean sellCard(Integer userId, Integer cardId) {

        System.out.println("[StoreService] StoreService#sellCard");

        UserDTO user = sender.getUserInfo(userId);
        CardDTO card = sender.getCardInfo(cardId);
        if (user == null || card == null) {
            return false;
        }

        if (card.getUserId() == null)
            return false;

        card.setUserId(null);
        sender.updateCard(card);
        user.setAccount(user.getAccount() + card.getPrice());
        sender.updateUser(user);

        return true;
    }

    public List<CardDTO> getAllStoreCard() {
        System.out.println("[StoreService] StoreService#getAllStoreCard");
        return sender.getAllCardsInfo();
    }
}
