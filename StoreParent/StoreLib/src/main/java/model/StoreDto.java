package model;

import java.io.Serializable;

public class StoreDto implements Serializable {

    private static final long serialVersionUID = 273379583247675049L;

    private Integer userId;
    private Integer cardId;

    public StoreDto() {
    }

    public StoreDto(Integer userId, Integer cardId) {
        this.userId = userId;
        this.cardId = cardId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCardId() {
        return cardId;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }

    @Override
    public String toString() {
        return "StoreDto{" +
                "userId=" + userId +
                ", cardId=" + cardId +
                '}';
    }
}
