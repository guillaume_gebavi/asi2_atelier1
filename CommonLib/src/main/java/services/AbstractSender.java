package services;

import dto.EnveloppeDto;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.jms.core.JmsTemplate;

import javax.annotation.PostConstruct;

@Log4j2
public abstract class AbstractSender {

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private Environment environment;

    private String queue;

    @PostConstruct
    public void init() {
        queue = environment.getProperty(getQueueKey());
    }

    protected void sendMessage(EnveloppeDto enveloppeDto, String key) {
        //log.info("Sending data...");
        jmsTemplate.convertAndSend(key, enveloppeDto);
    }

    public abstract String getQueueKey();
}
