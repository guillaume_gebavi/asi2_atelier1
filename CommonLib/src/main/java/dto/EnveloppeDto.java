package dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@NoArgsConstructor
public class EnveloppeDto<T> implements Serializable {

    @Getter
    @Setter
    private String objectType;

    @Getter
    private T object;

    public void setObject(T t) {
        this.object = t;
        if (this.objectType == null) {
            this.setObjectType(t.getClass().getName());
        }
    }

    public EnveloppeDto (T t) {
        setObject(t);
    }

    @Override
    public String toString() {
        return "EnveloppeDto{" +
                "objectType='" + objectType + '\'' +
                ", object=" + object +
                '}';
    }
}
