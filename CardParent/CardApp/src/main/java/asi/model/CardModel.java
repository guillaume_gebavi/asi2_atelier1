package asi.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Entity;

@Entity
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class CardModel extends CardReference {

    private float energy;
    private float hp;
    private float defence;
    private float attack;
    private float price;

    private Integer userId;
    private Integer storeId;

    public CardModel() {

    }

    public CardModel(CardDTO dto)
    {
        super(dto.getId(), dto.getName(), dto.getDescription(), dto.getFamily(), dto.getAffinity(), dto.getImgUrl(), dto.getSmallImgUrl());

        this.energy=dto.getEnergy();
        this.hp=dto.getHp();
        this.defence=dto.getDefence();
        this.attack=dto.getAttack();
        this.price=dto.getPrice();
        this.storeId = dto.getStoreId();
        this.userId = dto.getUserId();
    }

    public CardModel(CardModel cModel) {
        super(cModel);
        this.energy=cModel.getEnergy();
        this.hp=cModel.getHp();
        this.defence=cModel.getDefence();
        this.attack=cModel.getAttack();
        this.price=cModel.getPrice();
        this.storeId = cModel.getStoreId();
        this.userId = cModel.getUserId();
    }

    public CardModel(CardReference cardRef) {
        super(cardRef);
    }

    public float getEnergy() {
        return energy;
    }

    public void setEnergy(float energy) {
        this.energy = energy;
    }

    public float getHp() {
        return hp;
    }

    public void setHp(float hp) {
        this.hp = hp;
    }

    public float getDefence() {
        return defence;
    }

    public void setDefence(float defence) {
        this.defence = defence;
    }

    public float getAttack() {
        return attack;
    }

    public void setAttack(float attack) {
        this.attack = attack;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }
}
