package asi.MQServices;

import asi.model.CardDTO;
import asi.model.CardModel;
import dto.EnveloppeDto;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import services.AbstractSender;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Data
public class Sender extends AbstractSender {

    private static final String QUEUE_KEY = "fr.cpe.spring-app2.queue";
    @Override
    public String getQueueKey() {
        return QUEUE_KEY;
    }

    public void sendListCard(Set<Integer> set)
    {
        EnveloppeDto dto = new EnveloppeDto();
        dto.setObject(set);
        //dto.setObjectType(cardDTO.getClass().getName());


        System.out.println("[CardService] [ActiveMQ] Envoi du message sur 'User.Create.AskCard.Response.queue' : " + set);
        super.sendMessage(dto, "User.Create.AskCard.Response.queue");
    }

    public void sendMessage(CardDTO cardDTO) {

        EnveloppeDto dto = new EnveloppeDto();
        dto.setObject(cardDTO);
        //dto.setObjectType(cardDTO.getClass().getName());

        // Send a message
        System.out.println("[CardService] [ActiveMQ] Envoi du message sur 'User.Create.AskCard.queue' : " + cardDTO);
        super.sendMessage(dto, "User.Create.AskCard.queue");
    }

    public void sendListCardForUser(Set<Integer> list) {
        EnveloppeDto dto = new EnveloppeDto();
        dto.setObject(list);
        //dto.setObjectType(cardDTO.getClass().getName());

        // Send a message
        System.out.println("[CardService] [ActiveMQ] Envoi du message sur 'User.GetAllCards.Response.queue' : " + list);
        super.sendMessage(dto, "User.GetAllCards.Response.queue");
    }

    public void sendListCardForUserDto(List<CardDTO> listDto) {
        EnveloppeDto dto = new EnveloppeDto();
        dto.setObject(listDto);
        //dto.setObjectType(cardDTO.getClass().getName());

        // Send a message
        System.out.println("[CardService] [ActiveMQ] Envoi du message sur 'User.GetAllCards.DTO.Response.queue' : " + listDto);
        super.sendMessage(dto, "User.GetAllCards.DTO.Response.queue");
    }

    public void sendListCardToSell(List<CardDTO> listDto) {
        EnveloppeDto dto = new EnveloppeDto();
        dto.setObject(listDto);
        //dto.setObjectType(cardDTO.getClass().getName());

        // Send a message
        System.out.println("[CardService] [ActiveMQ] Envoi du message sur 'Cards.getCardsToSell.Response.queue' : " + listDto);
        super.sendMessage(dto, "Cards.getCardsToSell.Response.queue");
    }

    public void sendCard(CardDTO card) {
        EnveloppeDto dto = new EnveloppeDto();
        dto.setObject(card);
        //dto.setObjectType(cardDTO.getClass().getName());

        // Send a message
        System.out.println("[CardService] [ActiveMQ] Envoi du message sur 'Card.getCard.Response.queue' : " + card);
        super.sendMessage(dto, "Card.getCard.Response.queue");
    }
}
