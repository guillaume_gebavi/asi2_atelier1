package asi.MQServices;

import asi.Controller.CardModelService;
import asi.model.CardDTO;
import asi.model.CardModel;
import dto.EnveloppeDto;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import java.util.*;

@Log4j2
@Service
public class Receiver {

    @Autowired
    private Sender sender;

    @Autowired
    private CardModelService cardModelService;

    @JmsListener(destination = "User.Create.AskCard.queue")
    public void receiveMessage1(EnveloppeDto enveloppeDto) {
        Map data = (Map) enveloppeDto.getObject();

        System.out.println("[CardService] [ActiveMQ] Réception message sur 'User.Create.AskCard.queue' : " + data);

        List<CardModel> list = cardModelService.getRandCard((Integer) data.get("nb"), (Integer) data.get("id"));

        Set<Integer> set = new HashSet<>();
        for (CardModel model : list) {
            set.add(model.getId());
        }

        sender.sendListCard(set);

    }

    @JmsListener(destination = "User.GetAllCards.queue")
    public void receiveMessage2(EnveloppeDto enveloppeDto) {
        Integer userId = (Integer) enveloppeDto.getObject();

        System.out.println("[CardService] [ActiveMQ] Réception message sur 'User.GetAllCards.queue' : " + userId);

        List<CardModel> list = cardModelService.getAllCardForUserId(userId);
        List<CardDTO> listDto = new ArrayList<>();

        for (CardModel c : list) {
            CardDTO dto = new CardDTO(c.getId(), c.getName(), c.getDescription(), c.getFamily(), c.getAffinity(), c.getImgUrl(),
                    c.getSmallImgUrl(), c.getEnergy(), c.getHp(), c.getDefence(), c.getAttack(), c.getPrice(), c.getUserId(), c.getStoreId());
            listDto.add(dto);
        }

        Set<Integer> set = new HashSet<>();
        for (CardModel model : list) {
            set.add(model.getId());
        }

        sender.sendListCardForUser(set);


    }


    @JmsListener(destination = "User.GetAllCards.DTO.queue")
    public void receiveMessage3(EnveloppeDto enveloppeDto) {
        Integer userId = (Integer) enveloppeDto.getObject();

        System.out.println("[CardService] [ActiveMQ] Réception message sur 'User.GetAllCards.DTO.queue' : " + userId);

        List<CardModel> list = cardModelService.getAllCardForUserId(userId);
        List<CardDTO> listDto = new ArrayList<>();

        for (CardModel c : list) {
            CardDTO dto = new CardDTO(c.getId(), c.getName(), c.getDescription(), c.getFamily(), c.getAffinity(), c.getImgUrl(),
                    c.getSmallImgUrl(), c.getEnergy(), c.getHp(), c.getDefence(), c.getAttack(), c.getPrice(), c.getUserId(), c.getStoreId());
            listDto.add(dto);
        }


        sender.sendListCardForUserDto(listDto);


    }

    @JmsListener(destination = "Cards.getAllCards.queue")
    public void receiveMessage4(EnveloppeDto enveloppeDto) {

        System.out.println("[CardService] [ActiveMQ] Réception message sur 'Cards.getAllCards.queue'");

        List<CardModel> list = cardModelService.getAllCardToSell();
        List<CardDTO> listDto = new ArrayList<>();

        for (CardModel c : list) {
            CardDTO dto = new CardDTO(c.getId(), c.getName(), c.getDescription(), c.getFamily(), c.getAffinity(), c.getImgUrl(),
                    c.getSmallImgUrl(), c.getEnergy(), c.getHp(), c.getDefence(), c.getAttack(), c.getPrice(), c.getUserId(), c.getStoreId());
            listDto.add(dto);
        }

        sender.sendListCardToSell(listDto);
    }

    @JmsListener(destination = "Cards.updateCard.queue")
    public void receiveMessage5(EnveloppeDto dto) {

        log.info(dto);
        CardDTO cardDTO = (CardDTO) dto.getObject();

        System.out.println("[CardService] [ActiveMQ] Réception message sur 'Cards.updateCard.queue' : " + cardDTO);

        cardModelService.updateCard(new CardModel(cardDTO));

    }

    @JmsListener(destination = "Cards.getCard.queue")
    public void receiveMessage6(EnveloppeDto dto) {

        log.info(dto);
        Integer cardId = (Integer) dto.getObject();

        System.out.println("[CardService] [ActiveMQ] Réception message sur 'Cards.getCard.queue' : " + cardId);

        CardDTO cardDTO = null;
        Optional<CardModel> uModel = cardModelService.getCard(cardId);
        if (uModel.isPresent()) {
            CardModel c = uModel.get();

            cardDTO = new CardDTO(c.getId(), c.getName(), c.getDescription(), c.getFamily(), c.getAffinity(), c.getImgUrl(),
                    c.getSmallImgUrl(), c.getEnergy(), c.getHp(), c.getDefence(), c.getAttack(), c.getPrice(), c.getUserId(), c.getStoreId());
        }

        sender.sendCard(cardDTO);
    }

}
