package asi.Controller;

import asi.model.CardModel;
import asi.model.CardReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;


@Service
public class CardModelService {

    private Random rand;
    @Autowired
    private CardModelRepository cardRepository;
    @Autowired
    private CardReferenceService cardRefService;

    public CardModelService() {
        this.rand = new Random();
    }

    public List<CardModel> getAllCardModel() {

        System.out.println("[CardService] CardModelService#getAllCardModel");

        List<CardModel> cardList = new ArrayList<>();
        cardRepository.findAll().forEach(cardList::add);
        return cardList;
    }

    public void addCard(CardModel cardModel) {
        System.out.println("[CardService] CardModelService#addCard");
        cardRepository.save(cardModel);
    }

    public void updateCardRef(CardModel cardModel) {
        System.out.println("[CardService] CardModelService#updateCardRef");
        cardRepository.save(cardModel);
    }

    public void updateCard(CardModel cardModel) {
        System.out.println("[CardService] CardModelService#updateCard");
        cardRepository.save(cardModel);
    }

    public Optional<CardModel> getCard(Integer id) {
        System.out.println("[CardService] CardModelService#getCard");
        return cardRepository.findById(id);
    }

    public void deleteCardModel(Integer id) {
        System.out.println("[CardService] CardModelService#deleteCardModel");
        cardRepository.deleteById(id);
    }

    public List<CardModel> getRandCard(int nbr, int idUser) {
        System.out.println("[CardService] CardModelService#getRandCard");
        List<CardModel> cardList = new ArrayList<>();
        for (int i = 0; i < nbr; i++) {
            CardReference currentCardRef = cardRefService.getRandCardRef();
            CardModel currentCard = new CardModel(currentCardRef);
            currentCard.setAttack(rand.nextFloat() * 100);
            currentCard.setDefence(rand.nextFloat() * 100);
            currentCard.setEnergy(100);
            currentCard.setHp(rand.nextFloat() * 100);
            currentCard.setPrice(111);
            currentCard.setUserId(idUser);
            //save new card before sending for user creation
            cardRepository.save(currentCard);
            cardList.add(currentCard);
        }
        return cardList;
    }



    public List<CardModel> getAllCardToSell() {
        System.out.println("[CardService] CardModelService#getAllCardToSell");
        return this.cardRepository.findByUserId(null);
    }

    public List<CardModel> getAllCardForUserId(Integer userID) {
        System.out.println("[CardService] CardModelService#getAllCardForUserId");
        return this.cardRepository.findByUserId(userID);
    }
}

