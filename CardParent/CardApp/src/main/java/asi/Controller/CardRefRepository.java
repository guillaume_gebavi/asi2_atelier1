package asi.Controller;

import asi.model.CardReference;
import org.springframework.data.repository.CrudRepository;

public interface CardRefRepository extends CrudRepository<CardReference, Integer> {

}
