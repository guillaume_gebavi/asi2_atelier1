package asi.Controller;

import asi.model.CardDTO;
import asi.model.CardModel;
import asi.model.CardReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class CardRestController {


    @Autowired
    private CardModelService cardModelService;

    @Autowired
    private CardReferenceService cardReferenceService;

    @RequestMapping("/cards")
    private List<CardDTO> getAllCards() {
        List<CardDTO> cLightList = new ArrayList<>();
        for (CardModel c : cardModelService.getAllCardModel()) {
            CardDTO dto = new CardDTO(c.getId(), c.getName(), c.getDescription(), c.getFamily(), c.getAffinity(), c.getImgUrl(),
                    c.getSmallImgUrl(), c.getEnergy(), c.getHp(), c.getDefence(), c.getAttack(), c.getPrice(), c.getUserId(), c.getStoreId());

            cLightList.add(dto);
        }
        return cLightList;

    }

    @RequestMapping("/refcards")
    private List<CardDTO> getAllRefCards() {
        List<CardDTO> cLightList = new ArrayList<>();
        for (CardReference c : cardReferenceService.getAllCardRef()) {
            CardDTO dto = new CardDTO(c.getId(), c.getName(), c.getDescription(), c.getFamily(), c.getAffinity(), c.getImgUrl(),
                    c.getSmallImgUrl());

            cLightList.add(dto);
        }
        return cLightList;

    }

    @RequestMapping("/card/{id}")
    private CardDTO getCard(@PathVariable String id) {
        Optional<CardModel> rcard;
        rcard = cardModelService.getCard(Integer.valueOf(id));
        if (rcard.isPresent()) {
            CardModel c = rcard.get();

            CardDTO dto = new CardDTO(c.getId(), c.getName(), c.getDescription(), c.getFamily(), c.getAffinity(), c.getImgUrl(),
                    c.getSmallImgUrl(), c.getEnergy(), c.getHp(), c.getDefence(), c.getAttack(), c.getPrice(), c.getUserId(), c.getStoreId());

            return dto;
        }
        return null;

    }

    @RequestMapping(method = RequestMethod.POST, value = "/card")
    public void addCard(@RequestBody CardDTO card) {
        cardModelService.addCard(new CardModel((card)));
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/card/{id}")
    public void updateCard(@RequestBody CardDTO card, @PathVariable String id) {
        card.setId(Integer.valueOf(id));
        cardModelService.updateCard(new CardModel((card)));
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/card/{id}")
    public void deleteUser(@PathVariable String id) {
        cardModelService.deleteCardModel(Integer.valueOf(id));
    }

    @RequestMapping("/cards_to_sell")
    private List<CardDTO> getCardsToSell() {

        System.out.println("[CardService] CardRestController#getCardsToSell ");

        List<CardDTO> list = new ArrayList<>();
        for (CardModel c : cardModelService.getAllCardToSell()) {
            CardDTO dto = new CardDTO(c.getId(), c.getName(), c.getDescription(), c.getFamily(), c.getAffinity(), c.getImgUrl(),
                    c.getSmallImgUrl(), c.getEnergy(), c.getHp(), c.getDefence(), c.getAttack(), c.getPrice(), c.getUserId(), c.getStoreId());

            list.add(dto);
        }

        System.out.println("[CardService] CardRestController#getCardsToSell : retour (liste de CardDto) = " + list);

        return list;
    }

    @GetMapping("/getCardsForUser/{idUser}")
    private List<CardDTO> getCardsForUser(@PathVariable Integer idUser) {

        System.out.println("[CardService] CardRestController#getCardsForUser : idUser = " + idUser);

        List<CardDTO> list = new ArrayList<>();


        for (CardModel c :  cardModelService.getAllCardForUserId(idUser)) {
            CardDTO dto = new CardDTO(c.getId(), c.getName(), c.getDescription(), c.getFamily(), c.getAffinity(), c.getImgUrl(),
                    c.getSmallImgUrl(), c.getEnergy(), c.getHp(), c.getDefence(), c.getAttack(), c.getPrice(), c.getUserId(), c.getStoreId());

            list.add(dto);
        }

        System.out.println("[CardService] CardRestController#getCardsForUser : retour (liste de CardDto) = " + list);

        return list;
    }

}
